# Swayland

A clean & minimal build of Sway.

## Dependencies

-   [Sway](https://github.com/swaywm/sway) - Window Manager for Wayland
-   [Swaylock-Effects](https://github.com/mortie/swaylock-effects) - Lock Screen Manager for Sway
-   [Waybar](https://github.com/Alexays/Waybar) - Status Bar for Wayland
-   [Wofi](https://github.com/tsujp/wofi) - Menu & Launcher
-   [Alacritty](https://github.com/alacritty/alacritty) - Terminal Emulator
-   [Grim](https://github.com/emersion/grim) - Screenshot Utility
-	[Slurp](https://github.com/emersion/slurp) - Screenshot window selection tool
-   [Dunst](https://github.com/dunst-project/dunst) - Notification Daemon
-   [Playerctl](https://github.com/altdesktop/playerctl) - Media Player Controller
-   [Light](https://github.com/haikarainen/light) - Brightness Controller 
-   [XWayland](https://wayland.freedesktop.org/xserver.html) - X Clients under Wayland
-	[Autotiling](https://github.com/ammgws/autotiling-rs) - Autotiling for Sway
