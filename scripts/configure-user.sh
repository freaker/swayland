#!/bin/bash

if [ "$EUID" -eq 0 ]
    then echo "Do not run as root"
        exit
fi

echo
echo "======> Installing ZSH-Plugins"
git clone https://github.com/zsh-users/zsh-autosuggestions $HOME/.config/zsh/zsh-auto
git clone https://github.com/zdharma-continuum/fast-syntax-highlighting $HOME/.config/zsh/fsh

echo "======> Installing Inter font"
echo
mkdir -p $HOME/.local/share/
git clone https://github.com/ayush-rathore/inter-nerd-font $HOME/.local/share/fonts/

